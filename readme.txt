to start ensure:
vbox version VirtualBox-6.1.28-147628-Win installed
vagrant vagrant_2.2.18_i686.msi installed
once above done, do the following into command prompt (administrator privilleged command prompt not needed)
cd localdev
vagrant up

*vagrant-vbguest plugin that is stable and suitable to use is 0.21

once it is done, to get into vm shell type the following
vagrant ssh