#!/usr/bin/env python
"""url_mapper.py"""

import sys
import re

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    words = line.split(",")
    # increase counters
    if len(words) != 14:
        continue
    # write the results to STDOUT (standard output);
    # what we output here will be the input for the
    # Reduce step, i.e. the input for reducer.py
    #
    # Get URL
    #([s]?ftp|http[s]?)://(?:[a-zA-Z]|[0-9]|[$-_@.&+])[a-zA-Z0-9.-]+
    if len(words[3].strip()) > 0:
        match = re.search("([s]?ftp|http[s]?)://(?:[a-zA-Z]|[0-9]|[$-_@.&+])[a-zA-Z0-9.-]+", words[3])
        if match:
            print("%s\t%s" % (match.group(), 1))

