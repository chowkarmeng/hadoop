#!/usr/bin/env python
"""positive_reducer.py"""

import sys

current_iso = None
current_count = 0
sum_short_term_positive_rate = 0.0

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    iso, short_term_positive = line.split('\t')
    # convert short_term_positive (currently a string) to float
    try:
        short_term_positive_float = float(short_term_positive)
    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_iso == iso:
        current_count += 1
        sum_short_term_positive_rate += short_term_positive_float
    else:
        if current_iso:
            # write result to STDOUT
            avg = sum_short_term_positive_rate/current_count
            print("%s\t%s" % (current_iso, avg))
        # Initialize new key
        current_count = 1
        current_iso = iso
        sum_short_term_positive_rate = short_term_positive_float

# do not forget to output the last word if needed!
if current_iso == iso:
    avg = sum_short_term_positive_rate / current_count
    print("%s\t%s" % (current_iso, avg))
