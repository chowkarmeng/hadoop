#!/usr/bin/env python
"""positive_mapper.py"""

import sys

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    columns = line.split(",")
    # increase counters
    if len(columns) != 14:
        continue
    #Columns 2 Iso (alpha {3}) , Columns 13 Short-term positive rate (float)
    if len(columns[1].strip()) > 0 and len(columns[12].strip()) > 0:
        print("%s\t%s" % (columns[1].strip(), columns[12].strip()))
