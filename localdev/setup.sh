yum install -y vim telnet dos2unix git svn zip unzip mysql

# use docker-ce
yum install -y yum-utils device-mapper-persistent-data lvm2 
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io

# install development tools
yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm http://rpms.remirepo.net/enterprise/remi-release-7.rpm

systemctl enable docker
systemctl start docker

# Install Docker Compose
cp /vagrant/files/docker-compose /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose

# Disable firewall
systemctl disable firewalld
systemctl stop firewalld

# Disable selinux for docker
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

# Restart and refresh docker daemon 
systemctl restart docker

# Run docker compose, need to create a docker-compose file at ./../docker-compose.yml
#cd /data/docker/ && docker-compose down && docker-compose pull && docker-compose up -d