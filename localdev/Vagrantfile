# -*- mode: ruby -*-
# vi: set ft=ruby :# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
unless Vagrant.has_plugin?("vagrant-vbguest")
  exec "vagrant plugin install vagrant-vbguest"
end

unless Vagrant.has_plugin?("vagrant-hostmanager")
  exec "vagrant plugin install vagrant-hostmanager"
end

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|  # Every Vagrant virtual environment requires a box to build off of.
  
  config.vm.box = "centos/7"
  
  config.vm.define "docker-local"
  
  config.vm.hostname = "dockerized-local-vm"
  
  config.vm.network "private_network", type: "dhcp"  # If true, then any SSH connections made will enable agent forwarding.
  
  config.vm.network :forwarded_port, guest: 9870, host: 9870
  config.vm.network :forwarded_port, guest: 9000, host: 9000

  config.vm.provider "virtualbox" do |vb|
  
    # Boot with headless mode
    vb.gui = false    # Tweak the below value to adjust RAM
    vb.memory = 2048
  
    # Tweak the number of processors below
    vb.cpus = 2
 
    # Use VBoxManage to customize the VM. For example to change memory:
    config.vm.synced_folder ".", "/vagrant", type: "virtualbox"
	config.vm.synced_folder "../data", "/data", type: "virtualbox"
    
    # Configure hostfile for domains
    config.hostmanager.ip_resolver = proc do |vm, resolving_vm|
      if vm.id
        `"C:\\Program Files\\Oracle\\VirtualBox\\VBoxManage.exe" guestproperty get #{vm.id} "/VirtualBox/GuestInfo/Net/1/V4/IP"`.split()[1]
      end
    end

    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
    config.hostmanager.aliases = ["dockerized-local-vm.local"]
   
  end
  config.vm.provision :shell, :path => "setup.sh"
  config.vm.provision :shell, :path => "dockercomposeup.sh"
end
#C:\dev\VagrantQuickStartSample\VagrantQuickStartSample\localdev>vagrant up
#Bringing machine 'docker-local' up with 'virtualbox' provider...
#==> docker-local: Checking if box 'centos/7' version '1905.1' is up to date...
#==> docker-local: Clearing any previously set forwarded ports...
#==> docker-local: Clearing any previously set network interfaces...
#A host only network interface you're attempting to configure via DHCP
#already has a conflicting host only adapter with DHCP enabled. The
#DHCP on this adapter is incompatible with the DHCP settings. Two
#host only network interfaces are not allowed to overlap, and each
#host only network interface can have only one DHCP server. Please
#reconfigure your host only network or remove the virtual machine
#using the other host only network.
#https://github.com/hashicorp/vagrant/issues/3083